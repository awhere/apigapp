// taken from http://goo.gl/KuNFTv
function geocodeLocation() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet();
  var cells = sheet.getRangeByName("location");
  
  // Must have selected 3 columns (Location, Lat, Lng).
  // Must have selected at least 1 row.
  
  //if (cells.getNumColumns() != 3) {
  //  Logger.log("Must select the Location, Lat, Lng columns.");
  //  return;
  //}
  
  var addressColumn = 2;
  var addressRow = 1;
  
  var latRow = addressRow + 3;
  var lngRow = addressRow + 5;
  
  var geocoder = Maps.newGeocoder();
  var location;
  
  //for (addressRow = 1; addressRow <= cells.getNumRows(); ++addressRow) {
  address = cells.getCell(addressRow, addressColumn).getValue();
  
    // Geocode the address and plug the lat, lng pair into the
    // 2nd and 3rd elements of the current range row.
    location = geocoder.geocode(address);
  
    // Only change cells if geocoder seems to have gotten a
    // valid response.
    if (location.status == 'OK') {
      lat = location["results"][0]["geometry"]["location"]["lat"];
      lng = location["results"][0]["geometry"]["location"]["lng"];
            
      cells.getCell(latRow, addressColumn).setValue(lat);
      cells.getCell(lngRow, addressColumn).setValue(lng);
   }
  //}
};
