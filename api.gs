// Use username and apikey user provided in the spreadsheet
// To return api token and expire time
function get_token() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[0];

  // Get API Key and Secret to get a token
  var API_USERNAME = ss.getRangeByName("api_key").getValue();
  var API_SECRETKEY = ss.getRangeByName("api_secret").getValue();
  var url = "https://api.awhere.com/oauth/token";
  var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization" : "Basic " + Utilities.base64Encode(API_USERNAME + ":" + API_SECRETKEY)
  };

  var payload = {
    "grant_type": "client_credentials"
  };

  var options = {
    "method" : "POST",
    "headers" : headers,
    "payload": payload
  };

  var tokenJson = UrlFetchApp.fetch(url, options);
  var tokenData = JSON.parse(tokenJson);

  var timezone = Session.getScriptTimeZone();
  var d = new Date();
  d = new Date(d.getTime() + 1000*tokenData.expires_in);

  var formattedDate = Utilities.formatDate(d, timezone, "yyyy-MM-dd hh:mm:ss z");

  ss.getRangeByName("access_token").setValue(tokenData.access_token);
  ss.getRangeByName("expire_time").setValue(formattedDate);

  //sheet.getRange("F10").setValue(tokenData.access_token);
  //sheet.getRange("F11").setValue(tokenData.expires_in);

  return tokenData.access_token;
}

// Function for api token auto renewal
function token_renew() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[0];

  var API_TIME = ss.getRangeByName("expire_time").getValue()
  var year = +API_TIME.substring(0, 4);
  var month = +API_TIME.substring(6, 7);
  var day = +API_TIME.substring(9, 10);
  var hour = +API_TIME.substring(12, 13);
  var min = +API_TIME.substring(15,16);
  var sec = +API_TIME.substring(18,19);

  var time = new Date();

  var expire_time = new Date(year, month-1, day, hour, min, sec);

  // need to do something here to make this work
  if (expire_time < time ) {
    get_token();
  }
}

// Function to get the token
function retrieveToken() {

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  // var target = ss.getSheets()[1];
  var sheet = ss.getSheetByName("Output - Historical");

  // Get api token to use
  var API_TOKEN = ss.getRangeByName("access_token").getValue();

  return(API_TOKEN);
}

// Access daily observed api
function dailyObserved() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  // var target = ss.getSheets()[1];
  var sheet = ss.getSheetByName("Output - Historical");
  var lastRow = sheet.getLastRow();
  sheet.deleteRows(6, lastRow-6);

  // token_renew()

  // Get api token to use
  var API_TOKEN = ss.getRangeByName("access_token").getValue();

  // Find latitude and lng
  var cells = ss.getRangeByName("location");

  var addressColumn = 2;
  var addressRow = 1;

  var latRow = addressRow + 3;
  var lngRow = addressRow + 5;

  var lat = cells.getCell(latRow, addressColumn).getValue();
  var lng = cells.getCell(lngRow, addressColumn).getValue();

  // Find start date and end date
  var timezone = Session.getScriptTimeZone();
  var start = ss.getRangeByName("start_date").getValue();
  var end = ss.getRangeByName("end_date").getValue();

  start = Utilities.formatDate(start,timezone, "yyyy-MM-dd");
  end = Utilities.formatDate(end,timezone, "yyyy-MM-dd");

  var url = "https://api.awhere.com/v2/weather/locations/"+lat+","+lng+"/observations/"+start+","+end+"?limit=120";
  var headers = {
      "Authorization" : "Bearer " + API_TOKEN
  };

  var options = {
    "method" : "GET",
    "headers" : headers
  };

  // get the json
  var json = UrlFetchApp.fetch(url, options);
  var jsonData = JSON.parse(json).observations;

  // parse the json
  var rows = [],
      data;

  for (i = 0; i < jsonData.length; i++) {
    data = jsonData[i];
    rows.push([data.date, data.location.latitude, data.location.longitude, data.temperatures.max, data.temperatures.min, data.temperatures.units,
               data.precipitation.amount, data.precipitation.units, data.solar.amount, data.solar.units, data.relativeHumidity.max, data.relativeHumidity.min,
               data.wind.morningMax, data.wind.dayMax, data.wind.average, data.wind.units, data._links.self.href]); //your JSON entities here
  }

  dataRange = sheet.getRange(6, 2, rows.length, 17); // 3 Denotes total number of entites
  dataRange.setValues(rows);

}

// Access Agronomic api
function weatherNorms() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  // var target = ss.getSheets()[1];
  var sheet = ss.getSheetByName("Output - Norms");
  var lastRow = sheet.getLastRow();
  sheet.deleteRows(6, lastRow-6);

  // token_renew()

  // Get api token to use
  var API_TOKEN = ss.getRangeByName("access_token").getValue();

  // Find latitude and lng
  var cells = ss.getRangeByName("location");

  var addressColumn = 2;
  var addressRow = 1;

  var latRow = addressRow + 3;
  var lngRow = addressRow + 5;

  var lat = cells.getCell(latRow, addressColumn).getValue();
  var lng = cells.getCell(lngRow, addressColumn).getValue();

  // Find start date and end date
  var timezone = Session.getScriptTimeZone();
  var start = ss.getRangeByName("start_date").getValue();
  var end = ss.getRangeByName("end_date").getValue();

  start = Utilities.formatDate(start,timezone, "MM-dd");
  end = Utilities.formatDate(end,timezone, "MM-dd");

  var url = "https://api.awhere.com/v2/weather/locations/"+lat+","+lng+"/norms/"+start+","+end
  var headers = {
      "Authorization" : "Bearer " + API_TOKEN
  };

  var options = {
    "method" : "GET",
    "headers" : headers
  };

  // get the json
  var json = UrlFetchApp.fetch(url, options);
  var jsonData = JSON.parse(json).norms;

  // parse the json
  var rows = [],
      data;

  for (i = 0; i < jsonData.length; i++) {
    data = jsonData[i];
    rows.push([data.day,data.location.latitude,data.location.longitude,data.meanTemp.average,data.meanTemp.stdDev,data.meanTemp.units,data.maxTemp.average,data.maxTemp.stdDev,data.maxTemp.units,data.minTemp.average,
               data.minTemp.stdDev,data.minTemp.units,data.precipitation.average,data.precipitation.stdDev,data.precipitation.units,data.solar.average,data.solar.stdDev,data.solar.units,data.minHumidity.average,
               data.minHumidity.stdDev,data.maxHumidity.average,data.maxHumidity.stdDev,data.dailyMaxWind.average,data.dailyMaxWind.stdDev,data.dailyMaxWind.units,data.averageWind.average,data.averageWind.stdDev,
               data.averageWind.units,data._links.self.href]); //your JSON entities here
  }

  dataRange = sheet.getRange(6, 2, rows.length, 29); // 3 Denotes total number of entites
  dataRange.setValues(rows);

}

// Access Agronomic api
function agronomicsValue() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  // var target = ss.getSheets()[1];
  var sheet = ss.getSheetByName("Output - Agronomic");
  var lastRow = sheet.getLastRow();
  sheet.deleteRows(6, lastRow-6);

  // token_renew()

  // Get api token to use
  var API_TOKEN = ss.getRangeByName("access_token").getValue();

  // Find latitude and lng
  var cells = ss.getRangeByName("location");

  var addressColumn = 2;
  var addressRow = 1;

  var latRow = addressRow + 3;
  var lngRow = addressRow + 5;

  var lat = cells.getCell(latRow, addressColumn).getValue();
  var lng = cells.getCell(lngRow, addressColumn).getValue();

  // Find start date and end date
  var timezone = Session.getScriptTimeZone();
  var start = ss.getRangeByName("start_date").getValue();
  var end = ss.getRangeByName("end_date").getValue();

  start = Utilities.formatDate(start,timezone, "YYYY-MM-dd");
  end = Utilities.formatDate(end,timezone, "YYYY-MM-dd");

  var url = "https://api.awhere.com/v2/agronomics/locations/"+lat+","+lng+"/agronomicvalues/"+start+","+end;
  var headers = {
      "Authorization" : "Bearer " + API_TOKEN
  };

  var options = {
    "method" : "GET",
    "headers" : headers
  };

  // get the json
  var json = UrlFetchApp.fetch(url, options);
  var jsonData = JSON.parse(json).dailyValues;

  // parse the json
  var rows = [],
      data;

  for (i = 0; i < jsonData.length; i++) {
    data = jsonData[i];
    rows.push([data.date,data.gdd,data.ppet,data.accumulatedGdd,data.accumulatedPpet,data.pet.amount,data.pet.units,
               data.accumulatedPrecipitation.amount,data.accumulatedPrecipitation.units,data.accumulatedPet.amount,data.accumulatedPet.units,
               data._links.self.href]); //your JSON entities here
  }

  dataRange = sheet.getRange(6, 2, rows.length, 12); // 3 Denotes total number of entites
  dataRange.setValues(rows);

}

// Access Agronomic api
function agronomicsNorms() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  // var target = ss.getSheets()[1];
  var sheet = ss.getSheetByName("Output - Agronomic Norms");
  var lastRow = sheet.getLastRow();
  sheet.deleteRows(6, lastRow-6);

  // token_renew()

  // Get api token to use
  var API_TOKEN = ss.getRangeByName("access_token").getValue();

  // Find latitude and lng
  var cells = ss.getRangeByName("location");

  var addressColumn = 2;
  var addressRow = 1;

  var latRow = addressRow + 3;
  var lngRow = addressRow + 5;

  var lat = cells.getCell(latRow, addressColumn).getValue();
  var lng = cells.getCell(lngRow, addressColumn).getValue();

  // Find start date and end date
  var timezone = Session.getScriptTimeZone();
  var start = ss.getRangeByName("start_date").getValue();
  var end = ss.getRangeByName("end_date").getValue();

  start = Utilities.formatDate(start,timezone, "MM-dd");
  end = Utilities.formatDate(end,timezone, "MM-dd");

  var url = "https://api.awhere.com/v2/agronomics/locations/"+lat+","+lng+"/agronomicnorms/"+start+","+end;
  var headers = {
      "Authorization" : "Bearer " + API_TOKEN
  };

  var options = {
    "method" : "GET",
    "headers" : headers
  };

  // get the json
  var json = UrlFetchApp.fetch(url, options);
  var jsonData = JSON.parse(json).dailyNorms;

  // parse the json
  var rows = [],
      data;

  for (i = 0; i < jsonData.length; i++) {
    data = jsonData[i];
    rows.push([data.day,data.gdd.average,data.gdd.stdDev,data.pet.average,data.pet.stdDev,data.pet.units,data.ppet.average,data.ppet.stdDev,data.accumulatedGdd.average,data.accumulatedGdd.stdDev,
               data.accumulatedPrecipitation.average,data.accumulatedPrecipitation.stdDev,data.accumulatedPrecipitation.units,data.accumulatedPet.average,data.accumulatedPet.stdDev,data.accumulatedPet.units,
               data.accumulatedPpet.average,data.accumulatedPpet.stdDev,data._links.self.href]); //your JSON entities here
  }

  dataRange = sheet.getRange(6, 2, rows.length, 19); // 3 Denotes total number of entites
  dataRange.setValues(rows);

}

// Access forecast api
function forecast() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  // var target = ss.getSheets()[1];
  var sheet = ss.getSheetByName("Output - Forecast");
  var lastRow = sheet.getLastRow();
  sheet.deleteRows(6, lastRow-6);

  // token_renew()

  // Get api token to use
  var API_TOKEN = ss.getRangeByName("access_token").getValue();

  // Find latitude and lng
  var cells = ss.getRangeByName("location");

  var addressColumn = 2;
  var addressRow = 1;

  var latRow = addressRow + 3;
  var lngRow = addressRow + 5;

  var lat = cells.getCell(latRow, addressColumn).getValue();
  var lng = cells.getCell(lngRow, addressColumn).getValue();

  // Find start date and end date
  var date = new Date();
  var timezone = Session.getScriptTimeZone();

  date = Utilities.formatDate(date,timezone, "YYYY-MM-dd");

  var url = "https://api.awhere.com/v2/weather/locations/"+lat+","+lng+"/forecasts";
  var headers = {
      "Authorization" : "Bearer " + API_TOKEN
  };

  var options = {
    "method" : "GET",
    "headers" : headers
  };

  // get the json
  var json = UrlFetchApp.fetch(url, options);
  var jsonData = JSON.parse(json).forecasts;

  // parse the json
  var rows = [],
      data;

   for (i = 0; i < jsonData.length; i++) {
    data = jsonData[i].forecast;
     for (j = 0; j < data.length; j++) {
       d = data[j]
       rows.push([d.startTime,d.endTime,d.conditionsCode,d.conditionsText,d.temperatures.max,d.temperatures.min,d.temperatures.units,
               d.precipitation.chance,d.precipitation.amount,d.precipitation.units,d.sky.cloudCover,d.sky.sunshine,d.solar.amount,
               d.solar.units,d.relativeHumidity.average,d.wind.average,d.wind.units,d.dewPoint.amount,d.dewPoint.units]); //your JSON entities here
     }
   }

  dataRange = sheet.getRange(6, 2, rows.length, 19); // 3 Denotes total number of entites
  dataRange.setValues(rows);

}
