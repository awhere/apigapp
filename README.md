# Introduction
This holds the google app script for a Google app that gets data from the api and create visualizations accordingly. 

# Stage 1 of the app
Stage 1 of the app uses google apps scripts like excel macros to retrieve arguments in a google sheets and return api results in a tab. The visualization templates were set up before hand and update when the data is updated. 

The template can be accessed here: 
https://docs.google.com/a/awhere.com/spreadsheets/d/1tr6wVXvjPo7Ma1eo85Dk-7QmaJtyX9ollfksMuCXLOk/edit?usp=sharing

# Stage 2 of the app
A sidebars or drop down menu will be create. Once activated, a pop out dialog boxes to collect arguments to construct api calls. 

